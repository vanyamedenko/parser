<?php
/**
 * Created by Vanya Medenko.
 * User: Иван
 * Date: 10.11.2018
 * Time: 14:42
 */
class parser_class
{
    public static function runGettingInfo()
    {
        $enter_link = $_POST['link'];
        $link = file_get_html($enter_link);
        $link_title = $link->find('h1.page-header', 0)->innertext;
        $link_price = $link->find('[itemprop=price]', 0)->getAttribute('content');
        $image = $link->getElementById("mainImage");
        $image_link = $image->src;
        $array = array(
            "title" => $link_title,
            "price" => $link_price,
            "image" => $image_link);
        foreach ($array as $key => $value) {
            echo "{$key} => {$value}";
            echo "<br>";
        }
    }
}
